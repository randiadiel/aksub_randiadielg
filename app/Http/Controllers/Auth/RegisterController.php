<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Email;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        // $request->all();
        return Validator::make($data, [

            'nama' => ['required','string'],
            'email' => ['required', 'email', 'max:255', 'unique:emails,email'  ],
            'date'=> ['required'],
            'ktp' =>['required','mimes:pdf,jpg'],
            'phone' => ['required'],
            'address' => ['required', 'string'],
            'git' => ['required'],
            'provience' =>['required'],
            'nama1' => ['required','string'],
            'email1' => ['required', 'email', 'max:255', 'unique:emails,email' ],
            'date1'=> ['required'],
            'ktp1' =>['required','mimes:pdf,jpg'],
            'phone1' => ['required'],
            'address1' => ['required', 'string'],
            'git1' => ['required'],
            'provience1' =>['required'],
            'nama2' => ['required','string'],
            'email2' => ['required', 'email', 'max:255', 'unique:emails,email' ],
            'date2'=> ['required'],
            'ktp2' =>['required','mimes:pdf,jpg'],
            'phone2' => ['required'],
            'address2' => ['required', 'string'],
            'git2' => ['required'],
            'provience2' =>['required'],
            'group' => ['required','unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $request= Request();
        $users = $request -> all();
        $path = $request->file('ktp')->store('cv');
        $path1 = $request->file('ktp1')->store('cv');
        $path2 = $request->file('ktp2')->store('cv');
        
        User::create([
            'group'=>$request->group,
            'password'=>bcrypt($request->password),     
        ]);
        Email::create([
            'nama'=>$request->nama,
            'email'=>$request->email,
            'date'=>$request->date,
            'address'=>$request->address,
            'phone'=>$request->phone,
            'git'=>$request->git,
            'provience'=>$request->provience,
            'ktp'=>$path,
        ]);
        Email::create([
            'nama'=>$request->nama1,
            'email'=>$request->email1,
            'date'=>$request->date1,
            'address'=>$request->address1,
            'phone'=>$request->phone1,
            'git'=>$request->git1,
            'provience'=>$request->provience1,
            'ktp'=>$path,
        ]);
        Email::create([
            'nama'=>$request->nama2,
            'email'=>$request->email2,
            'date'=>$request->date2,
            'address'=>$request->address2,
            'phone'=>$request->phone2,
            'git'=>$request->git2,
            'provience'=>$request->provience2,
            'ktp'=>$path,
        ]);
        // save($data);
        return '/login';
    }

}
