<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $fillable = [
        'nama','date', 'address', 'ktp', 'phone', 'email', 'git', 'provience','user_id'
    ];
    public function user(){
        $this->belongsTo(App\User);
    }

}
