<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
  <meta charset="utf-8">
  <title></title>

</head>
<link rel="stylesheet" href="css/slick.css">
<link rel="stylesheet" href="css/slick-theme.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/styleVertical.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">

<body>
  <div id="loaderScreen">
    <div id="loaderItem">
      <img id="loaderImg" src="assets/Section1/logo-hackathon.png" alt="">
    </div>
  </div>
  <!-- END of Loading Screen -->
  <span id="topHeader" class="topHeader hilang">
    <form>
      <input id="hamburBar" type="checkbox" name="hamburBar" value="" onchange="hamburFunction()">
      <label for="hamburBar"><i class="fas fa-bars"></i></label>
    </form>
    <div class="">
      <img src="assets/Section1/logo-hackathon.png" alt="">
    <div class="">
      <z>BNCC</z>
      <z>HACKATHON</z>
    </div>
    </div>
    </span>
  <!-- Bottom Status Bar -->
  <div class="bottomstatus">
    <div class="progress-container">
      <div class="progress-bar" id="myBar"></div>
    </div>
  </div>
  <!-- End Bottom Status Bar -->
  <div class="navbarHome">
    <div id="menuPg" class="mobileMenu">
    <a href="#secOne"><h1>Start</h1></a>
    <a href="#secTwo"><h1>About Us</h1></a>
    <a href="#secSponsor"><h1>Sponsors</h1></a>
    <a href="#secMedpart"><h1>Media Partners</h1></a>
    <a href="#secFour"><h1>FAQ</h1></a>
    <a href="#secFour"><h1>Contact Us</h1></a>
    </div>
    <div id="blurPg" class="blurMenu active"></div>
  </div>
  
  <div id="horizontalBody" class="horizontal" data-0="transform:translateX(0%)" data-4000="transform:translateX(-308%)">
    <!-- Section 1 Start -->
    <div id="secOne" class="bgsecOne">
      <div class="mainContainer secOne">
        <img id="bg-secOne" class="bg1" src="" alt="">
        <div class="containers">
          <div class="Heading">
            <div class="headerLogo">
              <img src="assets/Section1/logo-hackathon.png" alt="">
              <div class="">
                <h1>BNCC</h1>
                <h1>HACKATHON</h1>
              </div>
            </div>
            <h3>"Code Your <b>Idea</b>, Discover Your <b>Future</b>"</h3>
            <h1>Hackathon</h1>
            <h1>Competition</h1>
            <h1>Day</h1>
            <h2>Will Be Coming at</h2>
            <h1 id="tanggalBox"><b>11th - 13th July </b>2019</h1>
          </div>
          <div class="Register">
            <img src="assets/Section1/Section1grphc.png" alt="">
            <h3>Join now and turn your idea into a real thing</h3>
            <div style="display: flex;">
              <form action="/register">
                <input class="regButton" type="submit" value="Register" />
              </form>
              <form action="/login">
                <input class="regButton" type="submit" value="Login" />
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- Section 1 End -->
    </div>

    <!-- Section 2 Start -->
    <div id="secTwo" class="secTwo container">
      <div class="row innerSecTwo">
        <div class="col-lg-12 col-xl-3 quarterSection">
          <img src="assets/Section2/Background/backgroundSec2.png" class="halo">
          <img src="assets/Section2/IsometricCity.png" class="isoCity" alt="">
          <img src="assets/Section2/Star.png" class="stars" alt="">
          <div class="aboutSection">
            <h2>About BNCC Hackathon</h2>
            <p style="color: white;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex
              ea commodo consequat.
              cillum dolore e cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          </div>
        </div>
        <div class="col-lg-12 col-xl-3 lightBalls quarterSection">
          <div class="flexRow">
            <div class="flexImg">
              <img src="assets/Section2/Programmer.png" class="imgProgrammer">
              <h2>Why You Should Join ?</h2>
            </div>
            <div class="flexContent">
              <div class="">
                <img src="assets/Section2/Icon/Domain.png" alt="">
                <div class="">
                  <h3>Domain</h3>
                  <p>Lorem ipsum dolor sit ameod tempor uat. Duis aute i Lorem ipsum dolor sit ameod temrurunt mollit anim id est laborum.</p>
                </div>
              </div>
              <div class="">
                <img src="assets/Section2/Icon/Hosting.png" alt="">
                <div class="">
                  <h3>Mentor</h3>
                  <p>Lorem ipsum dolor sit ameod Lorem ipsum dolor sit ameod tem tempo jfa irurunt mollit anim id est laborum.</p>
                </div>
              </div>
              <div class="">
                <img src="assets/Section2/Icon/Job Interview.png" alt="">
                <div class="">
                  <h3>Hosting</h3>
                  <p>Lorem ipsum dolor sit ameod tempodfakkkkda Lorem ipsum dolor sit ameod teml ld mollit anim id est laborum.</p>
                </div>
              </div>
              <div class="">
                <img src="assets/Section2/Icon/Mentor.png" alt="">
                <div class="">
                  <h3>Job Interview</h3>
                  <p>Lorem ipsum dolor sit ameod tempor incioremaUt enim irurunt mo Lorem ipsum dolor sit ameod temllit anim id est laborum.</p>
                </div>
              </div>
            </div>
          </div>


        </div>
        <div class="col-lg-12 col-xl-3 Section3 quarterSection">
          <img id="tendImg" src="assets/Section3/Group 38.png" alt="">
          <div class="regisCarousel">
            <div class="carr">
              <div class="">
                <!-- carousell Satu -->
                <div class="content">
                  <img src="assets/Section3/Icon/Registration.png" alt="Regis">
                  <div>
                    <h2>Registration</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur a cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

                  </div>
                </div>
              </div>
              <div class="">
                <!-- carousell Dua -->
                <div class="content">
                  <img src="assets/Section3/Icon/Registration.png" alt="Regis">
                  <div>
                    <h2>Registration</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur a cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                  </div>
                </div>
              </div>
              <div class="">
                <div class="content">
                  <img src="assets/Section3/Icon/Registration.png" alt="Regis">
                  <div>
                    <h2>Registration</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur a cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-12 col-xl-3 quarterSection">
          <img src="assets/Section3/challenge.png" class="clg" style="left: 10%!important;" alt="">

          <div class="clgSec">
            <h2>The Challenge</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
              Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          </div>
        </div>
      </div>
      <!-- Lower Path -->
      <img src="assets/LowerGraphic/pathbottom-rev.png" style="width:213vw;height: 100vh;" class="footPath">
      <!-- end Lower Path -->
    </div>
    <!-- <div id="secThree" class="secThree">
      <div class="spNmpBox">
        <div id="secSponsor" class="sponsorBox">
          <div class="caption">
            <h1>Sponsors</h1>
          </div>
        </div>
        <div id="secMedpart" class="mediaBox">
          <div class="caption">
            <h1>Media Partners</h1>
          </div>

        </div>
      </div>
    </div> -->
    <div id="secFour" class="secFour">
      <div class="row">
        <div class="col-xl-6 height-faq">
          <span class="tanda-tanya"><i class="fas fa-question-circle tanda-tanya"></i></span>
          <h1>FAQ</h1>
          <div class="row upper">
            <div class="col-lg-6 baris-faq">
              <div class="question">
                <div class="title-q">
                  <div class="row-lg-9 state-tq">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  </div>
                  <div class="row-lg-3 logo-tq" id="js-logo"><i class="fas fa-angle-down " id="angle"></i></div>
                </div>
                <div class="drop-q hidden" id="js-hidden">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                  consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                  cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                  proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </div>
              </div>
              <div class="question">
                <div class="title-q">
                  <div class="row-lg-9 state-tq">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  </div>
                  <div class="row-lg-3 logo-tq" id="js-logo1"><i class="fas fa-angle-down " id="angle1"></i></div>
                </div>
                <div class="drop-q hidden " id="js-hidden1">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                  consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                  cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                  proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </div>
              </div>
              <div class="question">
                <div class="title-q">
                  <div class="row-lg-9 state-tq">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  </div>
                  <div class="row-lg-3 logo-tq" id="js-logo2"><i class="fas fa-angle-down" id="angle2"></i></div>
                </div>
                <div class="drop-q hidden" id="js-hidden2">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                  consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                  cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                  proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </div>
              </div>
              <div class="question">
                <div class="title-q">
                  <div class="row-lg-9 state-tq">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  </div>
                  <div class="row-lg-3 logo-tq" id="js-logo3"><i class="fas fa-angle-down " id="angle3"></i></div>
                </div>
                <div class="drop-q hidden " id="js-hidden3">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                  consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                  cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                  proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </div>
              </div>
              <div class="question">
                <div class="title-q">
                  <div class="row-lg-9 state-tq">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  </div>
                  <div class="row-lg-3 logo-tq" id="js-logo4"><i class="fas fa-angle-down " id="angle4"></i></div>
                </div>
                <div class="drop-q hidden" id="js-hidden4">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                  consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                  cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                  proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </div>
              </div>



            </div>
            <div class="col-lg-6 baris-faq">
              <div class="question">
                <div class="title-q">
                  <div class="row-lg-9 state-tq">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  </div>
                  <div class="row-lg-3 logo-tq" id="js-logo5"><i class="fas fa-angle-down" id="angle5"></i></div>
                </div>
                <div class="drop-q hidden" id="js-hidden5">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo

                </div>
              </div>
              <div class="question">
                <div class="title-q">
                  <div class="row-lg-9 state-tq">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  </div>
                  <div class="row-lg-3 logo-tq" id="js-logo6"><i class="fas fa-angle-down" id="angle6"></i></div>
                </div>
                <div class="drop-q hidden " id="js-hidden6">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo

                </div>
              </div>
              <div class="question">
                <div class="title-q">
                  <div class="row-lg-9 state-tq">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  </div>
                  <div class="row-lg-3 logo-tq" id="js-logo7"><i class="fas fa-angle-down" id="angle7"></i></div>
                </div>
                <div class="drop-q hidden" id="js-hidden7">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo

                </div>
              </div>
              <div class="question">
                <div class="title-q">
                  <div class="row-lg-9 state-tq">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  </div>
                  <div class="row-lg-3 logo-tq" id="js-logo8"><i class="fas fa-angle-down" id="angle8"></i></div>
                </div>
                <div class="drop-q hidden " id="js-hidden8">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo

                </div>
              </div>
              <div class="question">
                <div class="title-q">
                  <div class="row-lg-9 state-tq">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  </div>
                  <div class="row-lg-3 logo-tq" id="js-logo9"><i class="fas fa-angle-down" id="angle9"></i></div>
                </div>
                <div class="drop-q hidden" id="js-hidden9">
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                  tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                  quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo

                </div>
              </div>



            </div>

            <div class="row below">
              <p>Have other question?</p>
              <p>Contact Us</p>
            </div>
          </div>

        </div>

        <div class="col-xl-6 fourHalf container">
          <div class="col-12 maps">
            <h1>Location</h1>
            <div class="locContainer">
              <div class="locMaps">
                <div id="map" class=""></div>
              </div>
              <div class="locDetails">
                <h4>The Competition Will Be will be held at Tokopedia Tower</h4>
                <p>Tokopedia Tower Ciputra World 2, Jl. Prof. DR. Satrio No.Kav. 11, RT.3/RW.3, Karet Semanggi, Setia Budi, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12950</p>

              </div>
            </div>
          </div>
          <div class="col-12 maps">
            <h1>Contact Us</h1>
            <div class="row contactUs">
              <div class="col-lg-7 col-12 formContact">
                <form class="form-group" action="{{route('welcome')}}" method="post">
                    @csrf
                  <input type="text" name="nama" value="" placeholder="Name">
                  @if($errors->has('nama'))
                    <span class="invalid-feedback" role="alert"> 
                        <strong>{{$errors->first('nama')}}</strong>
                    </span>
                  @endif  
                  <input type="email" name="email" value="" placeholder="Email">
                  @if($errors->has('email'))
                    <span class="invalid-feedback" role="alert"> 
                        <strong>{{$errors->first('email')}}</strong>
                    </span>
                  @endif  
                  <input type="text" name="subject" value="" placeholder="Subject">
                  @if($errors->has('subject'))
                    <span class="invalid-feedback" role="alert"> 
                        <strong>{{$errors->first('subject')}}</strong>
                    </span>
                  @endif  
                  <textarea name="message" rows="5" placeholder="Your Messages..."></textarea>
                  @if($errors->has('message'))
                    <span class="invalid-feedback" role="alert"> 
                        <strong>{{$errors->first('message')}}</strong>
                    </span>
                  @endif  
                  <input class="btn btn-success" type="submit" name="" value="Submit">
                </form>
              </div>
              <div class="col-lg-5 col-12">
                <div class="sosmedCard">
                  <h5>You Can Reach Us too from:</h5>
                  <ul>
                    <li><i class="fab fa-line"></i>
                      <p>test123</p>
                    </li>
                    <li><i class="fab fa-whatsapp-square"></i>
                      <p>+6285882777788</p>
                    </li>
                    <li><i class="fab fa-facebook"></i>
                      <p>Hackathon</p>
                    </li>
                    <li><i class="fab fa-instagram"></i>
                      <p>@Hackathon</p>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="secFooter ">
      <div class="logo">
        <div class="">
          <p>Powered By</p>
          <img src="assets/Footer/BINUS.png" alt="">
        </div>
        <div class="">
          <p>Organized By</p>
          <img src="assets/Footer/BNCC.png" alt="">
        </div>
      </div>
      <div class="copyText">
        <p class="">2019 &copy; Bina Nusantara Computer Club</p>
      </div>
    </div>
  </div>
  </div>
  <!-- Section 2 End -->

  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
  <script type="text/javascript" src="js/skrollr.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <!-- <script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js?ver=1.3.2'></script> -->
  <script type="text/javascript" src="js/slick.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCF3juWRf-aD_8-JHJWe0C8k7ISe-O646s&libraries=places&callback=initMap" async defer></script>
  <script type="text/javascript">
    window.addEventListener("load", function() {
      var load_screen = document.getElementById("loaderScreen");
      load_screen.classList.add('animated', 'fadeOut');
      setTimeout(function() {
        load_screen.classList.add('hilang');
      }, 1000);
    });
  </script>
  <script type="text/javascript">
    const elt = document.getElementById('loaderImg');
    elt.classList.add('animated', 'bounce', 'infinite', 'slower');
  </script>
  <script type="text/javascript">
    function hamburFunction(){
      var hamburbar = document.getElementById('hamburBar');
      var menuPg = document.getElementById('menuPg');
      if (hamburBar.checked == true) {
        menuPg.style = ('z-index: 2;');
        menuPg.classList.remove('animated','slideOutLeft');
        menuPg.classList.add('animated','slideInLeft');
        blur.classList.remove('active');
      }
      else {
        menuPg.style = ('margin-left: 0px;');
        menuPg.classList.remove('animated','slideInLeft');
        menuPg.classList.add('animated','slideOutLeft');
        blur.classList.add('active');
      }
    }
    var blur = document.getElementById('blurPg');
    var hamburbar = document.getElementById('hamburBar');
    var menuPg = document.getElementById('menuPg');

    blur.addEventListener('click',function(){
        menuPg.style = ('margin-left: 0px;');
        menuPg.classList.remove('animated','slideInLeft');
        menuPg.classList.add('animated','slideOutLeft');
        blur.classList.add('active');
        document.getElementById("hamburBar").checked = false;
    })

  </script> 
  <script type="text/javascript">
    var nilai = 1200;

    function faqMobile() {
      let a = document.getElementById('js-hidden');
      let c = document.getElementById('js-logo');
      let b = document.getElementById('js-hidden1');
      let d = document.getElementById('js-logo1');

      let e = document.getElementById('js-hidden2');
      let g = document.getElementById('js-logo2');
      let f = document.getElementById('js-hidden3');
      let h = document.getElementById('js-logo3');

      let i = document.getElementById('js-hidden4');
      let k = document.getElementById('js-logo4');

      let ac = document.getElementById('angle');
      let bd = document.getElementById('angle1');
      let eg = document.getElementById('angle2');
      let fh = document.getElementById('angle3');
      let ik = document.getElementById('angle4');

      let j = document.getElementById('js-hidden5');
      let m = document.getElementById('js-logo5');
      let l = document.getElementById('js-hidden6');
      let n = document.getElementById('js-logo6');

      let o = document.getElementById('js-hidden7');
      let q = document.getElementById('js-logo7');
      let p = document.getElementById('js-hidden8');
      let r = document.getElementById('js-logo8');

      let s = document.getElementById('js-hidden9');
      let t = document.getElementById('js-logo9');

      let jm = document.getElementById('angle5');
      let ln = document.getElementById('angle6');
      let oq = document.getElementById('angle7');
      let pr = document.getElementById('angle8');
      let st = document.getElementById('angle9');

      c.addEventListener('click', function() {

        ac.classList.add('rotate-90');
        if (a.classList.contains('hidden')) {
          a.classList.remove('hidden');
          b.classList.add('hidden');
          e.classList.add('hidden');
          f.classList.add('hidden');
          i.classList.add('hidden');

          s.classList.add('hidden');
          j.classList.add('hidden');
          o.classList.add('hidden');
          p.classList.add('hidden');
          l.classList.add('hidden');

          bd.classList.remove('rotate-90');
          eg.classList.remove('rotate-90');
          fh.classList.remove('rotate-90');
          ik.classList.remove('rotate-90');

          jm.classList.remove('rotate-90');
          ln.classList.remove('rotate-90');
          oq.classList.remove('rotate-90');
          pr.classList.remove('rotate-90');
          st.classList.remove('rotate-90');
        } else {
          a.classList.add('hidden');
          ac.classList.remove('rotate-90');
        }
      })



      d.addEventListener('click', function() {
        bd.classList.add('rotate-90');
        if (b.classList.contains('hidden')) {

          b.classList.remove('hidden');
          a.classList.add('hidden');
          e.classList.add('hidden');
          f.classList.add('hidden');
          i.classList.add('hidden');

          s.classList.add('hidden');
          j.classList.add('hidden');
          o.classList.add('hidden');
          p.classList.add('hidden');
          l.classList.add('hidden');

          ac.classList.remove('rotate-90');
          eg.classList.remove('rotate-90');
          fh.classList.remove('rotate-90');
          ik.classList.remove('rotate-90');

          jm.classList.remove('rotate-90');
          ln.classList.remove('rotate-90');
          oq.classList.remove('rotate-90');
          pr.classList.remove('rotate-90');
          st.classList.remove('rotate-90');
        } else {
          b.classList.add('hidden');
          bd.classList.remove('rotate-90');

        }

      })
      g.addEventListener('click', function() {
        eg.classList.add('rotate-90');
        if (e.classList.contains('hidden')) {

          e.classList.remove('hidden');
          a.classList.add('hidden');
          b.classList.add('hidden');
          f.classList.add('hidden');
          i.classList.add('hidden');

          s.classList.add('hidden');
          j.classList.add('hidden');
          o.classList.add('hidden');
          p.classList.add('hidden');
          l.classList.add('hidden');

          ac.classList.remove('rotate-90');
          bd.classList.remove('rotate-90');
          fh.classList.remove('rotate-90');
          ik.classList.remove('rotate-90');

          jm.classList.remove('rotate-90');
          ln.classList.remove('rotate-90');
          oq.classList.remove('rotate-90');
          pr.classList.remove('rotate-90');
          st.classList.remove('rotate-90');
        } else {
          e.classList.add('hidden');
          eg.classList.remove('rotate-90');
        }

      })
      h.addEventListener('click', function() {
        fh.classList.add('rotate-90');
        if (f.classList.contains('hidden')) {

          f.classList.remove('hidden');
          a.classList.add('hidden');
          e.classList.add('hidden');
          b.classList.add('hidden');
          i.classList.add('hidden');

          s.classList.add('hidden');
          j.classList.add('hidden');
          o.classList.add('hidden');
          p.classList.add('hidden');
          l.classList.add('hidden');

          ac.classList.remove('rotate-90');
          bd.classList.remove('rotate-90');
          eg.classList.remove('rotate-90');
          ik.classList.remove('rotate-90');

          jm.classList.remove('rotate-90');
          ln.classList.remove('rotate-90');
          oq.classList.remove('rotate-90');
          pr.classList.remove('rotate-90');
          st.classList.remove('rotate-90');
        } else {
          f.classList.add('hidden');
          fh.classList.remove('rotate-90');
        }

      })
      k.addEventListener('click', function() {

        ik.classList.add('rotate-90');
        if (i.classList.contains('hidden')) {

          i.classList.remove('hidden');
          a.classList.add('hidden');
          e.classList.add('hidden');
          f.classList.add('hidden');
          b.classList.add('hidden');

          s.classList.add('hidden');
          j.classList.add('hidden');
          o.classList.add('hidden');
          p.classList.add('hidden');
          l.classList.add('hidden');

          ac.classList.remove('rotate-90');
          bd.classList.remove('rotate-90');
          eg.classList.remove('rotate-90');
          fh.classList.remove('rotate-90');

          jm.classList.remove('rotate-90');
          ln.classList.remove('rotate-90');
          oq.classList.remove('rotate-90');
          pr.classList.remove('rotate-90');
          st.classList.remove('rotate-90');
        } else {
          i.classList.add('hidden');
          ik.classList.remove('rotate-90');
        }

      })



      m.addEventListener('click', function() {

        jm.classList.add('rotate-90');
        if (j.classList.contains('hidden')) {
          j.classList.remove('hidden');
          l.classList.add('hidden');
          o.classList.add('hidden');
          p.classList.add('hidden');
          s.classList.add('hidden');

          i.classList.add('hidden');
          a.classList.add('hidden');
          e.classList.add('hidden');
          f.classList.add('hidden');
          b.classList.add('hidden');

          ln.classList.remove('rotate-90');
          oq.classList.remove('rotate-90');
          pr.classList.remove('rotate-90');
          st.classList.remove('rotate-90');

          ik.classList.remove('rotate-90');
          ac.classList.remove('rotate-90');
          bd.classList.remove('rotate-90');
          eg.classList.remove('rotate-90');
          fh.classList.remove('rotate-90');

        } else {
          j.classList.add('hidden');
          jm.classList.remove('rotate-90');
        }
      })



      n.addEventListener('click', function() {
        ln.classList.add('rotate-90');
        if (l.classList.contains('hidden')) {

          l.classList.remove('hidden');
          j.classList.add('hidden');
          o.classList.add('hidden');
          p.classList.add('hidden');
          s.classList.add('hidden');

          i.classList.add('hidden');
          a.classList.add('hidden');
          e.classList.add('hidden');
          f.classList.add('hidden');
          b.classList.add('hidden');

          jm.classList.remove('rotate-90');
          oq.classList.remove('rotate-90');
          pr.classList.remove('rotate-90');
          st.classList.remove('rotate-90');

          ik.classList.remove('rotate-90');
          ac.classList.remove('rotate-90');
          bd.classList.remove('rotate-90');
          eg.classList.remove('rotate-90');
          fh.classList.remove('rotate-90');
        } else {
          l.classList.add('hidden');
          ln.classList.remove('rotate-90');
        }

      })
      q.addEventListener('click', function() {
        oq.classList.add('rotate-90');
        if (o.classList.contains('hidden')) {

          o.classList.remove('hidden');
          j.classList.add('hidden');
          l.classList.add('hidden');
          p.classList.add('hidden');
          s.classList.add('hidden');

          i.classList.add('hidden');
          a.classList.add('hidden');
          e.classList.add('hidden');
          f.classList.add('hidden');
          b.classList.add('hidden');

          jm.classList.remove('rotate-90');
          pr.classList.remove('rotate-90');
          st.classList.remove('rotate-90');
          ln.classList.remove('rotate-90');

          ik.classList.remove('rotate-90');
          ac.classList.remove('rotate-90');
          bd.classList.remove('rotate-90');
          eg.classList.remove('rotate-90');
          fh.classList.remove('rotate-90');
        } else {
          o.classList.add('hidden');
          oq.classList.remove('rotate-90');
        }

      })
      r.addEventListener('click', function() {
        pr.classList.add('rotate-90');
        if (p.classList.contains('hidden')) {

          p.classList.remove('hidden');
          j.classList.add('hidden');
          o.classList.add('hidden');
          l.classList.add('hidden');
          s.classList.add('hidden');

          i.classList.add('hidden');
          a.classList.add('hidden');
          e.classList.add('hidden');
          f.classList.add('hidden');
          b.classList.add('hidden');

          jm.classList.remove('rotate-90');
          st.classList.remove('rotate-90');
          oq.classList.remove('rotate-90');
          ln.classList.remove('rotate-90');

          ik.classList.remove('rotate-90');
          ac.classList.remove('rotate-90');
          bd.classList.remove('rotate-90');
          eg.classList.remove('rotate-90');
          fh.classList.remove('rotate-90');
        } else {
          p.classList.add('hidden');
          pr.classList.remove('rotate-90');
        }

      })
      t.addEventListener('click', function() {
        st.classList.add('rotate-90');
        if (s.classList.contains('hidden')) {

          s.classList.remove('hidden');
          j.classList.add('hidden');
          o.classList.add('hidden');
          p.classList.add('hidden');
          l.classList.add('hidden');

          i.classList.add('hidden');
          a.classList.add('hidden');
          e.classList.add('hidden');
          f.classList.add('hidden');
          b.classList.add('hidden');

          jm.classList.remove('rotate-90');
          ln.classList.remove('rotate-90');
          oq.classList.remove('rotate-90');
          pr.classList.remove('rotate-90');

          ik.classList.remove('rotate-90');
          ac.classList.remove('rotate-90');
          bd.classList.remove('rotate-90');
          eg.classList.remove('rotate-90');
          fh.classList.remove('rotate-90');
        } else {
          s.classList.add('hidden');
          st.classList.remove('rotate-90');
        }

      })
    }
    skrollr.init();
    if (skrollr.init().isMobile()) {
      skrollr.init().destroy();
      document.getElementById('bg-secOne').src = 'assets/Section1/background1-mob.png';
      document.getElementById('topHeader').classList.remove('hilang');
      faqMobile();


    } else {
      if (window.innerWidth < nilai) {
        skrollr.init().destroy();
        document.getElementById('topHeader').classList.remove('hilang');
        document.getElementById('bg-secOne').src = 'assets/Section1/background1-mob.png';

        faqMobile();
      } else {
        skrollr.init();
        document.getElementById('topHeader').classList.add('hilang');
        document.getElementById('bg-secOne').src = 'assets/Section1/background1.png';

        let a = document.getElementById('js-hidden');
        let c = document.getElementById('js-logo');
        let b = document.getElementById('js-hidden1');
        let d = document.getElementById('js-logo1');

        let e = document.getElementById('js-hidden2');
        let g = document.getElementById('js-logo2');
        let f = document.getElementById('js-hidden3');
        let h = document.getElementById('js-logo3');

        let i = document.getElementById('js-hidden4');
        let k = document.getElementById('js-logo4');

        let ac = document.getElementById('angle');
        let bd = document.getElementById('angle1');
        let eg = document.getElementById('angle2');
        let fh = document.getElementById('angle3');
        let ik = document.getElementById('angle4');

        c.addEventListener('click', function() {

          ac.classList.add('rotate-90');
          if (a.classList.contains('hidden')) {
            a.classList.remove('hidden');
            b.classList.add('hidden');
            e.classList.add('hidden');
            f.classList.add('hidden');
            i.classList.add('hidden');

            bd.classList.remove('rotate-90');
            eg.classList.remove('rotate-90');
            fh.classList.remove('rotate-90');
            ik.classList.remove('rotate-90');
          } else {
            a.classList.add('hidden');
            ac.classList.remove('rotate-90');
          }
        })



        d.addEventListener('click', function() {
          bd.classList.add('rotate-90');
          if (b.classList.contains('hidden')) {

            b.classList.remove('hidden');
            a.classList.add('hidden');
            e.classList.add('hidden');
            f.classList.add('hidden');
            i.classList.add('hidden');

            ac.classList.remove('rotate-90');
            eg.classList.remove('rotate-90');
            fh.classList.remove('rotate-90');
            ik.classList.remove('rotate-90');
          } else {
            b.classList.add('hidden');
            bd.classList.remove('rotate-90');

          }

        })
        g.addEventListener('click', function() {
          eg.classList.add('rotate-90');
          if (e.classList.contains('hidden')) {

            e.classList.remove('hidden');
            a.classList.add('hidden');
            b.classList.add('hidden');
            f.classList.add('hidden');
            i.classList.add('hidden');

            ac.classList.remove('rotate-90');
            bd.classList.remove('rotate-90');
            fh.classList.remove('rotate-90');
            ik.classList.remove('rotate-90');
          } else {
            e.classList.add('hidden');
            eg.classList.remove('rotate-90');
          }

        })
        h.addEventListener('click', function() {
          fh.classList.add('rotate-90');
          if (f.classList.contains('hidden')) {

            f.classList.remove('hidden');
            a.classList.add('hidden');
            e.classList.add('hidden');
            b.classList.add('hidden');
            i.classList.add('hidden');

            ac.classList.remove('rotate-90');
            bd.classList.remove('rotate-90');
            eg.classList.remove('rotate-90');
            ik.classList.remove('rotate-90');
          } else {
            f.classList.add('hidden');
            fh.classList.remove('rotate-90');
          }

        })
        k.addEventListener('click', function() {

          ik.classList.add('rotate-90');
          if (i.classList.contains('hidden')) {

            i.classList.remove('hidden');
            a.classList.add('hidden');
            e.classList.add('hidden');
            f.classList.add('hidden');
            b.classList.add('hidden');

            ac.classList.remove('rotate-90');
            bd.classList.remove('rotate-90');
            eg.classList.remove('rotate-90');
            fh.classList.remove('rotate-90');
          } else {
            i.classList.add('hidden');
            ik.classList.remove('rotate-90');
          }

        })

        let j = document.getElementById('js-hidden5');
        let m = document.getElementById('js-logo5');
        let l = document.getElementById('js-hidden6');
        let n = document.getElementById('js-logo6');

        let o = document.getElementById('js-hidden7');
        let q = document.getElementById('js-logo7');
        let p = document.getElementById('js-hidden8');
        let r = document.getElementById('js-logo8');

        let s = document.getElementById('js-hidden9');
        let t = document.getElementById('js-logo9');

        let jm = document.getElementById('angle5');
        let ln = document.getElementById('angle6');
        let oq = document.getElementById('angle7');
        let pr = document.getElementById('angle8');
        let st = document.getElementById('angle9');

        m.addEventListener('click', function() {

          jm.classList.add('rotate-90');
          if (j.classList.contains('hidden')) {
            j.classList.remove('hidden');
            l.classList.add('hidden');
            o.classList.add('hidden');
            p.classList.add('hidden');
            s.classList.add('hidden');

            ln.classList.remove('rotate-90');
            oq.classList.remove('rotate-90');
            pr.classList.remove('rotate-90');
            st.classList.remove('rotate-90');

          } else {
            j.classList.add('hidden');
            jm.classList.remove('rotate-90');
          }
        })



        n.addEventListener('click', function() {
          ln.classList.add('rotate-90');
          if (l.classList.contains('hidden')) {

            l.classList.remove('hidden');
            j.classList.add('hidden');
            o.classList.add('hidden');
            p.classList.add('hidden');
            s.classList.add('hidden');

            jm.classList.remove('rotate-90');
            oq.classList.remove('rotate-90');
            pr.classList.remove('rotate-90');
            st.classList.remove('rotate-90');
          } else {
            l.classList.add('hidden');
            ln.classList.remove('rotate-90');
          }

        })
        q.addEventListener('click', function() {
          oq.classList.add('rotate-90');
          if (o.classList.contains('hidden')) {

            o.classList.remove('hidden');
            j.classList.add('hidden');
            l.classList.add('hidden');
            p.classList.add('hidden');
            s.classList.add('hidden');

            jm.classList.remove('rotate-90');
            pr.classList.remove('rotate-90');
            st.classList.remove('rotate-90');
            ln.classList.remove('rotate-90');
          } else {
            o.classList.add('hidden');
            oq.classList.remove('rotate-90');
          }

        })
        r.addEventListener('click', function() {
          pr.classList.add('rotate-90');
          if (p.classList.contains('hidden')) {

            p.classList.remove('hidden');
            j.classList.add('hidden');
            o.classList.add('hidden');
            l.classList.add('hidden');
            s.classList.add('hidden');

            jm.classList.remove('rotate-90');
            st.classList.remove('rotate-90');
            oq.classList.remove('rotate-90');
            ln.classList.remove('rotate-90');
          } else {
            p.classList.add('hidden');
            pr.classList.remove('rotate-90');
          }

        })
        t.addEventListener('click', function() {
          st.classList.add('rotate-90');
          if (s.classList.contains('hidden')) {

            s.classList.remove('hidden');
            j.classList.add('hidden');
            o.classList.add('hidden');
            p.classList.add('hidden');
            l.classList.add('hidden');

            jm.classList.remove('rotate-90');
            ln.classList.remove('rotate-90');
            oq.classList.remove('rotate-90');
            pr.classList.remove('rotate-90');
          } else {
            s.classList.add('hidden');
            st.classList.remove('rotate-90');
          }

        })

      }
      window.addEventListener('resize', function() {
        if (window.innerWidth < nilai) {
          document.getElementById('topHeader').classList.remove('hilang');
          skrollr.init().destroy();
          document.getElementById('bg-secOne').src = 'assets/Section1/background1-mob.png';
        } else {
          skrollr.init();
          document.getElementById('topHeader').classList.add('hilang');
          document.getElementById('bg-secOne').src = 'assets/Section1/background1.png';
        }
      });
    }
  </script>
  <script type="text/javascript">
    $('.carr').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 5000,
      dots: true,
      infinite: true
    });
  </script>
  <script type="text/javascript">
    window.onscroll = function() {
      myFunction()
    };

    function myFunction() {
      var screenChg = 10;
      var winScroll = document.body.scrollTop || document.documentElement.scrollTop;
      var height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
      var scrolled = (winScroll / height) * 100;
      var topHeader = document.getElementById("topHeader");
      document.getElementById("myBar").style.width = scrolled + "%";
      // if (scrolled > screenChg) {
      //   topHeader.classList.remove('hilang', 'slideOutLeft');
      //   if (window.innerWidth < 1200) {} else {
      //     topHeader.classList.add('animated', 'slideInLeft');
      //   }
      // }
      // if (scrolled < screenChg) {
      //   if (window.innerWidth > 1200) {
      //     topHeader.classList.remove('slideInLeft');
      //     topHeader.classList.add('animated', 'slideOutLeft');
      //     topHeader.classList.add('hilang');
      //   }
      // }
    }
  </script>
  <script>
    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    var map;
    var service;
    var infowindow;

    function initMap() {
      var sydney = new google.maps.LatLng(48.494320, 11.937000);

      infowindow = new google.maps.InfoWindow();

      map = new google.maps.Map(
        document.getElementById('map'), {
          center: sydney,
          zoom: 15
        });

      var request = {
        query: 'Wang Express',
        fields: ['name', 'geometry'],
      };

      service = new google.maps.places.PlacesService(map);

      service.findPlaceFromQuery(request, function(results, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
          for (var i = 0; i < results.length; i++) {
            createMarker(results[i]);
          }

          map.setCenter(results[0].geometry.location);
        }
      });
    }

    function createMarker(place) {
      var marker = new google.maps.Marker({
        map: map,
        position: place.geometry.location
      });

      google.maps.event.addListener(marker, 'click', function() {
        infowindow.setContent(place.name);
        infowindow.open(map, this);
      });
    }
  </script>
</body>

</html>
