<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>

	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1" name="viewport" />
	<title>Login</title>
	<link rel="stylesheet" type="text/css" href="css/loginstyle.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.min.js"></script>
</head>
<body>
	<div id="scene" class="bgImg">
    <div data-depth="0.3" class="">
    </div>
  </div>
	<div class="bodi">
		<div class="contentAll">
		 <img src="assets/register/logo-hackathon.png" alt="">
		 <h2>BNCC HACKATHON</h2>
      	 <h3>Code Your Idea, Discover Your Future</h3>

		 	<div class="contentForm">
		 		<h1>Login</h1>
                 <form method="POST" action="{{ route('login') }}">
                 @csrf

		 		<hr>
		 		<div class="login">
		 			<div class="login-item">
		 				<input id="email" placeholder="Group Name" type="text" class="form-control{{ $errors->has('group') ? ' is-invalid' : '' }}" name="group" value="{{ old('group') }}" required autofocus>

                                @if ($errors->has('group'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('group') }}</strong>
                                    </span>
                                @endif
								<i class="fas fa-users"></i>
		 				<h3> | </h3>
		 			</div>
		 			<div class="login-item">
                     <input id="password" placeholder="Password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
								<i class="fas fa-key"></i>
		 				<h3> | </h3>
		 			</div>
                     <button type="submit" class="btLogin">
                                    {{ __('Login') }}
                                </button>
		 		</div>
                 </form>

                 @if (Route::has('password.request'))
                                    <a class="Forget" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif


		 	</div>

		 	<div class="contentForm">
		 		<div class="bawahLogin">
		 			<a class="Account" href="/register">Don't have an account yet? click here!</a>
		 			<div class="logo-sosmed">
					 	<a href="https://www.facebook.com/" target="_blank"><i class="fab fa-facebook-f"></i></a>
						<a href="https://www.instagram.com/" target="_blank"><i class="fab fa-instagram"></i></a>

		 			</div>
		 		</div>


		 	</div>

		</div>
	</div>
	<script type="text/javascript">
	var scene = document.getElementById('scene');
	var parallaxInstance = new Parallax(scene);
	</script>
</body>
</html>
