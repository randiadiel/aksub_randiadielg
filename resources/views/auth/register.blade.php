<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>

	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1" name="viewport" />
	<title>Register</title>
	<link rel="stylesheet" type="text/css" href="css/regisStyle.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	  <script src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.min.js"></script>
</head>
<div id="scene" class="bgImg">
    <div data-depth="0.3" class="">
    </div>
  </div>
  <div class="bodi">
    <div class="containerAll">
      <img src="assets/register/logo-hackathon.png" alt="">
      <h2>BNCC HACKATHON</h2>
      <h2>Code Your Idea, Discover Your Future</h2>
      <h3>Registration Deadline: 11 May 2019</h3>
      <br>
      <form method="POST" action="{{ route('register') }}" enctype="multipart/form-data">
      @csrf
      <div class="containerForm">
        <h1>{{ __('Register') }}</h1>
        <hr>
        <h2>Participants</h2>


        <div class="leader">
          <h3>Leader</h3>
          <div class="innerLeader">
            <div class="leader-input">
            <input id="nama" type="text" class="form-control{{ $errors->has('nama') ? ' is-invalid' : '' }}" name="nama" value="{{ old('nama') }}" placeholder="Name" required>
              <i class="fas fa-user"></i>
              <h3>
                |
              </h3>
            </div>
            @if ($errors->has('nama'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('nama') }}</strong>
                </span>
            @endif
            <div class="leader-input">
            <input id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ old('address') }}" placeholder="Address" required autofocus>
              <i class="fas fa-map-marked-alt"></i>
              <h3>
                |
              </h3>
            </div>
            @if ($errors->has('address'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
            <div class="leader-input">
            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" required>
              <i class="fas fa-envelope"></i>
              <h3>
                |
              </h3>
            </div>
            @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
            <div class="leader-input">
            <input id="phone" type="tel" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}" placeholder="WhatsApp Number" required autofocus><i class="fab fa-whatsapp"></i>
              <h3>
                |
              </h3>
            </div>
            @if ($errors->has('phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
            <div class="leader-input">
            <input id="git" type="text" class="form-control{{ $errors->has('git') ? ' is-invalid' : '' }}" name="git" value="{{ old('git') }}" placeholder="GitLab" required autofocus>
              <i class="fab fa-gitlab"></i>
              <h3>
                |
              </h3>
            </div>
            @if ($errors->has('git'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('git') }}</strong>
                                    </span>
                                @endif
            <div class="inputBirth">
              <div class="">
                <label>Birth Date</label>
                <input id="date" type="date" class="form-control{{ $errors->has('date') ? ' is-invalid' : '' }}" name="date" value="{{ old('date') }}" placeholder="Birth Date" required autofocus>
                @if ($errors->has('date'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('date') }}</strong>
                    </span>
                @endif
              </div>
              <div class="">
                <label>Birth Place</label>
                <input id="provience" type="text" class="form-control{{ $errors->has('provience') ? ' is-invalid' : '' }}" name="provience" value="{{ old('provience') }}" placeholder="Birth Place" required autofocus>
              </div>
              @if ($errors->has('provience'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('provience') }}</strong>
                                    </span>
                                @endif
            </div>
            <label for="Upload File">Upload ID Card</label>
            <div class="leader-input">
            <input id="ktp" type="file" class="form-control{{ $errors->has('ktp') ? ' is-invalid' : '' }}" name="ktp" value="{{ old('ktp') }}" placeholder="User ID" required autofocus>
              <i class="fas fa-folder"></i>
              <h3>
                |
              </h3>
            </div>
            @if ($errors->has('ktp'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('ktp') }}</strong>
                                    </span>
                                @endif
          </div>



        </div>
        <!--member1-->
        <div class="leader">
          <div style="display: flex;flex-direction: row;">
            <h3>Member 1</h3>
            <button type="button" id="buttonHilang1" class="opencloseButton rotate45" onclick="openmember1()"><img src="assets/register/Icon/addAndDel.png" alt=""></button>

          </div>
          <div id="hidden" class="innerLeader ">
            <div class="leader-input">
            <input id="nama" type="text" class="form-control{{ $errors->has('nama') ? ' is-invalid' : '' }}" name="nama1" value="{{ old('nama') }}" placeholder="Nama" required>

              <i class="fas fa-user"></i>
              <h3>
                |
              </h3>
            </div>
            @if ($errors->has('nama'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nama') }}</strong>
                                    </span>
                                @endif
            <div class="leader-input">
            <input id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address1" value="{{ old('address') }}" placeholder="Address" required autofocus>
              <i class="fas fa-map-marked-alt"></i>
              <h3>
                |
              </h3>
            </div>
            @if ($errors->has('address'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
            <div class="leader-input">
            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email1" value="{{ old('email') }}" placeholder="Email" required>
              <i class="fas fa-envelope"></i>
              <h3>
                |
              </h3>
            </div>
            @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
            <div class="leader-input">
            <input id="phone" type="tel" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone1" value="{{ old('phone') }}" placeholder="WhatsApp Number" required autofocus>
              <i class="fab fa-whatsapp"></i>
              <h3>
                |
              </h3>
            </div>
            @if ($errors->has('phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
            <div class="leader-input">
            <input id="git" type="text" class="form-control{{ $errors->has('git') ? ' is-invalid' : '' }}" name="git1" value="{{ old('git') }}" placeholder="GitLab" required autofocus>
              <i class="fab fa-gitlab"></i>
              <h3>
                |
              </h3>
            </div>
            @if ($errors->has('git'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('git') }}</strong>
                                    </span>
                                @endif
            <div class="inputBirth">
              <div class="">
                <label>Birth Date</label>
                <input id="date" type="date" class="form-control{{ $errors->has('date') ? ' is-invalid' : '' }}" name="date1" value="{{ old('date') }}" placeholder="Birth Date" required autofocus>
              </div>
              @if ($errors->has('date'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('date') }}</strong>
                                    </span>
                                @endif
              <div class="">
                  <label>Birth Place</label>
                  <input id="provience" type="text" class="form-control{{ $errors->has('provience') ? ' is-invalid' : '' }}" name="provience1" value="{{ old('provience') }}" placeholder="Birth Place" required autofocus>
              </div>
              @if ($errors->has('provience'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('provience') }}</strong>
                                    </span>
                                @endif
            </div>
            <label>Input ID Card</label>
            <div class="leader-input">
            <input id="ktp" type="file" class="form-control{{ $errors->has('ktp') ? ' is-invalid' : '' }}" name="ktp1" value="{{ old('ktp') }}" placeholder="User ID" required autofocus>
              <i class="fas fa-folder"></i>
              <h3>
                |
              </h3>
            </div>
            @if ($errors->has('ktp'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('ktp') }}</strong>
                                    </span>
                                @endif
            <!--member2-->
            <div class="leader member2">
              <div style="display: flex;flex-direction: row;">
                <h3>Member 2</h3>
                <button type="button" id="buttonHilang2" class="opencloseButton rotate45" onclick="openmember2()"><img src="assets/register/Icon/addAndDel.png" alt=""></button>

              </div>
              <div id="hidden2" class="innerLeader ">
                <div class="leader-input">
                <input id="nama" type="text" class="form-control{{ $errors->has('nama') ? ' is-invalid' : '' }}" name="nama2" value="{{ old('nama') }}" placeholder="Name" required>
                  <i class="fas fa-user"></i>
                  <h3>
                    |
                  </h3>
                </div>
                @if ($errors->has('nama'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nama') }}</strong>
                                    </span>
                                @endif
                <div class="leader-input">
                <input id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address2" value="{{ old('address') }}" placeholder="Address" required autofocus>
                  <i class="fas fa-map-marked-alt"></i>
                  <h3>
                    |
                  </h3>
                </div>
                @if ($errors->has('address'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                <div class="leader-input">
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email2" value="{{ old('email') }}" placeholder="Email" required>
                  <i class="fas fa-envelope"></i>
                  <h3>
                    |
                  </h3>
                </div>
                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                <div class="leader-input">
                <input id="phone" type="tel" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone2" value="{{ old('phone') }}" placeholder="WhatsApp Number" required autofocus>
                  <i class="fab fa-whatsapp"></i>
                  <h3>
                    |
                  </h3>
                </div>
                @if ($errors->has('phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                <div class="leader-input">
                <input id="git" type="text" class="form-control{{ $errors->has('git') ? ' is-invalid' : '' }}" name="git2" value="{{ old('git') }}" placeholder="GitLab" required autofocus>
                  <i class="fab fa-gitlab"></i>
                  <h3>
                    |
                  </h3>
                </div>
                @if ($errors->has('git'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('git') }}</strong>
                                    </span>
                                @endif
                <div class="inputBirth">
                  <div class="">
                    <label>Birth Date</label>
                    <input id="date" type="date" class="form-control{{ $errors->has('date') ? ' is-invalid' : '' }}" name="date2" value="{{ old('date') }}" placeholder="Birth Date" required autofocus>
                  </div>
                  @if ($errors->has('date'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('date') }}</strong>
                                    </span>
                                @endif
                  <div class="">
                    <label>Birth Place</label>
                    <input id="provience" type="text" class="form-control{{ $errors->has('provience') ? ' is-invalid' : '' }}" name="provience2" value="{{ old('provience') }}" placeholder="Birth Place" required autofocus>
                  </div>
                  @if ($errors->has('provience'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('provience') }}</strong>
                                    </span>
                                @endif
                </div>
                <label>Input ID Card</label>
                <div class="leader-input">
                <input id="ktp" type="file" class="form-control{{ $errors->has('ktp') ? ' is-invalid' : '' }}" name="ktp2" value="{{ old('ktp') }}" placeholder="User ID" required autofocus>
                  <i class="fas fa-envelope"></i>
                  <h3>
                    |
                  </h3>
                </div>
                @if ($errors->has('ktp'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('ktp') }}</strong>
                                    </span>
                                @endif
              </div>



            </div>

          </div>



        </div>

        <hr>
        <h2>Group</h2>
        <div class="leader">
          <div class="innerLeader">
            <div class="leader-input">
            <input id="group" type="text" class="form-control{{ $errors->has('group') ? ' is-invalid' : '' }}" name="group" value="{{ old('group') }}" placeholder="Group Name" required autofocus>
              <i class="fas fa-users"></i>
              <h3>
                |
              </h3>
            </div>
            @if ($errors->has('group'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('group') }}</strong>
                                    </span>
                                @endif
            <div class="leader-input">
            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>
              <i class="fas fa-key"></i>
              <h3>
                |
              </h3>
            </div>
            @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
            <div class="leader-input">
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirm Password" required>
              <i class="far fa-check-circle"></i>
              <h3>
                |
              </h3>
            </div>
          </div>
        <input class="submitButton" type="submit" name="" value="Register">
      </div>
    </form>
    </div>
    <div class="contentForm">
      <div class="bawahLogin">
        <a class="Account" href="/login">Already have an account? click here!</a>
        <div class="logo-sosmed">
          <a href="https://www.facebook.com/" target="_blank"><i class="fab fa-facebook-f"></i></a>
          <a href="https://www.instagram.com/" target="_blank"><i class="fab fa-instagram"></i></a>

        </div>
      </div>
  </div>
  <script type="text/javascript">
    function openmember1() {
      var visible = document.getElementById("hidden");
      var buttonVisible = document.getElementById("buttonHilang1");

      if (visible.classList.contains('hilang')) {
        visible.classList.remove('hilang');
        buttonVisible.classList.remove('rotate45');
      } else {
        visible.classList.add('hilang');
        buttonVisible.classList.add('rotate45');
      }
    }

    function openmember2() {
      var visible = document.getElementById("hidden2");
      var buttonVisible2 = document.getElementById("buttonHilang2");

      if (visible.classList.contains('hilang')) {
        visible.classList.remove('hilang');
        buttonVisible2.classList.remove('rotate45');
      } else {
        visible.classList.add('hilang');
        buttonVisible2.classList.add('rotate45');
      }
    }
  </script>
  <script type="text/javascript">
  var scene = document.getElementById('scene');
  var parallaxInstance = new Parallax(scene);
  </script>
</body>
</html>
